etcd Cookbook
==================

This cookbook will download and install etcd into `/opt/` and lets you run etcd under [runit][runit], [docker][docker] or [rocket][rocket]


Requirements
------------

Running a OS that is supported by the `etcd` binary [releases][etcd_releases]  
+ If you wish to use Docker this lwrp is designed for [chef-docker][chef-docker]

Resources and Providers
--------------------------

This cookbook provides several resources and corresponding providers.

`install.rb`
----------

Install etcd with this resource

Actions:

* `install` - installs _etcd_ into `/opt`
* `remove` - removes _etcd_ from `/opt`

### etcd::install

| Attribute  | Type  | Description  | Default  | Required |
|---|---|---|---|---|
| version  | String  | Version of _etcd_ to install  | unset  | *yes*  |
| checksum | String | Checksum to tarball | unset | *no* |
| uri | String | URI to download _etcd_ from | `https://github.com/coreos/etcd/releases/download/` | *yes* |

---
`start.rb`

Start _etcd_ with this resource

Actions:

* `runit` - starts _etcd_ using [runit][runit]
* `rocket` - starts _etcd_ using [rocket][rocket]
* `docker` - starts _etcd_ using [docker][docker]

### etcd::start

| Attribute  | Type  | Description  | Default  | Required |
|---|---|---|---|---|
| version  | String  | Version of _etcd_ to install  | unset  | *yes*  |
| runtemplatename | String | Filename to use as an erb template to run _etcd_ | etcd | *yes* |
| cookbook | String | Cookbook that holds the erb templlate | etcd | *yes* |
| port | String | Default port for etcd to listen on for containers | 4001:4001 | *yes* |
| tag | String | Tag name for the Container Image | unset | *yes* for containers |

---

`pull.rb`

Pull an _etcd_ image with this resource

Actions:

* `docker` pulls an _etcd_ image using [docker][docker]
* `rocket` pulls an _etcd_ image using [rocket][rocket]

### etcd::pull


| Attribute  | Type  | Description  | Default  | Required |
|---|---|---|---|---|
| name | String | Name of the container to pull| unset | *yes* |
| tag | String | Tag name for the Container Image | unset | *yes* |

---
`stop.rb`

Stop _etcd_ with this resource

Actions:

* `runit` - stops _etcd_ using [runit][runit]
* `docker` - stops _etcd_ using [docker][docker]
* `rocket` - stops _etcd_ using [rocket][rocket]

| Attribute  | Type  | Description  | Default  | Required |
|---|---|---|---|---|
| name | String | Name of the container | unset | *yes* |

| tag | String | Tag name for the Container Image | unset | *yes* for containers |

---

Usage
-----

You should write a wrapper cookbook to interface with this; you can use the _tests_ as a example.

```ruby
include_recipe "runit"
etcd_install node.name do
  version "2.0.5"
  action :install
end
etcd_start node.name do
  version "2.0.5"
  action :runit
end
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Author:: Scott M. Likens <scott@spam.likens.us>

Copyright 2015, Scott M. Likens

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[runit]: http://smarden.org/runit/
[docker]: https://www.docker.com/
[rocket]: https://github.com/coreos/rocket
[etcd_releases]: https://github.com/coreos/etcd/releases
[chef-docker]: https://github.com/bflad/chef-docker
