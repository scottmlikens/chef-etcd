action :runit do
  runit_service "etcd" do
    run_template_name new_resource.runtemplatename
    cookbook new_resource.cookbook
    default_logger true
    options(
      arguments: new_resource.arguments,
      prefix: "/opt/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? "amd64" : "i386"}"
    )
  end
end
action :docker do
  docker_container new_resource.name do
    detach true
    port new_resource.port
    tag new_resource.tag
    env new_resource.arguments
  end
end
