action :runit do
  runit_service "etcd" do
    action :stop
  end
end
action :docker do
  docker_container new_resource.name do
    action :stop
  end
end
