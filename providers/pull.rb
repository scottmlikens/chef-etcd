def whyrun_supported?
  true
end

use_inline_resources if defined?(use_inline_resources)

action :docker do
  docker_image new_resource.name do
    tag new_resource.tag
  end
end

action :rocket do
  rocket_image new_resource.name
end
