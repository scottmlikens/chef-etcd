def whyrun_supported?
  true
end

use_inline_resources if defined?(use_inline_resources)

action :install do
  remote_file "#{Chef::Config[:file_cache_path]}/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? 'amd64' : 'i386'}.tar.gz" do
    source "#{new_resource.uri}v#{new_resource.version}/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? "amd64" : "i386"}.tar.gz"
    checksum checksum
    action :create_if_missing
  end
  execute "untar etcd" do
    cwd Chef::Config[:file_cache_path]
    command "tar zxfv #{Chef::Config[:file_cache_path]}/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? 'amd64' : 'i386'}.tar.gz -C /opt"
    creates "/opt/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? 'amd64' : 'i386'}/etcd"
  end
end

action :remove do
  directory "/opt/etcd-v#{new_resource.version}-#{node['os']}-#{node['kernel']['machine'] =~ /x86_64/ ? 'amd64' : 'i386'}" do
    action :delete
    recursive true
  end
end
