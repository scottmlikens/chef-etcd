#
# Cookbook Name:: chef-etcd_test
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

etcd_install node.name do
  action :install
  version "2.0.5"
end

etcd_install node.name do
  action :remove
  version "2.0.5"
end
