describe "etcd" do
  describe_recipe "etcd_test::default" do
    include MiniTest::Chef::Assertions
    include MiniTest::Chef::Context
    include MiniTest::Chef::Resources
    describe "it should have etcd installed" do
      it "should have etcd in /opt" do
        file("/opt/etcd-v2.0.5-linux-amd64/etcd").must_exist
      end
    end
  end
end
