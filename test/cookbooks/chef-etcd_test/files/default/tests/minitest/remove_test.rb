describe "etcd" do
  describe_recipe "etcd_test::remove" do
    include MiniTest::Chef::Assertions
    include MiniTest::Chef::Context
    include MiniTest::Chef::Resources
    describe "it should not have etcd installed" do
      it "should have etcd in /opt" do
        file("/opt/etcd-v2.0.5-linux-amd64/etcd").wont_exist
      end
    end
  end
end
