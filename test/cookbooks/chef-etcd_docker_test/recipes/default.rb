#
# Cookbook Name:: chef-etcd_docker_test
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package "curl"
include_recipe 'docker::aufs'
include_recipe 'docker::package'
etcd_pull "quay.io/coreos/etcd" do
  action :docker
  tag "v2.0.5"
end
blah=Array.new
blah << "ETCD_HEARTBEAT_INTERVAL=100"
blah << "ETCD_ELECTION_TIMEOUT=500"
etcd_start "quay.io/coreos/etcd" do
  action :docker
  tag "v2.0.5"
  arguments blah
end
