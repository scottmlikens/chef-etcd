#
# Cookbook Name:: chef-etcd_test
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "runit"
etcd_install node.name do
  action :install
  version "2.0.5"
end
etcd_args=Array.new
etcd_args << "-heartbeat-interval=100"
etcd_args << "-election-timeout=500"
etcd_start node.name do
  action :runit
  version "2.0.5"
  arguments etcd_args
end
