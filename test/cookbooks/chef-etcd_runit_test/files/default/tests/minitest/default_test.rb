describe "etcd" do
  describe_recipe "etcd runit::default" do
    include MiniTest::Chef::Assertions
    include MiniTest::Chef::Context
    include MiniTest::Chef::Resources
    it "should have etcd in /opt" do
      file("/opt/etcd-v2.0.5-linux-amd64/etcd").must_exist
    end
    it "should have etcd running under runit" do
      service("etcd").must_be_running
    end
  end
end
