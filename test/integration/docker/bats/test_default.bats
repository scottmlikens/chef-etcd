#!/usr/bin/env bats
@test "etcd container should exist" {
docker inspect etcd
}
@test "we should be able to talk to it on 4001" {
curl localhost:4001
}