actions :docker, :rocket

attribute :name, :kind_of => String, :required => true
attribute :tag, :kind_of => String
