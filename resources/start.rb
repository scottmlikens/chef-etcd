actions :docker, :runit, :rocket

attribute :arguments, :kind_of => Array
attribute :version, :kind_of => String, :required => true
attribute :cookbook, :kind_of => String, :default => "etcd", :required => true
attribute :runtemplatename, :kind_of => String, :default => "etcd", :required => true
attribute :port, :kind_of => String, :default => "4001:4001"
attribute :tag, :kind_of => String, :required => false
