actions :install, :remove

attribute :version, :kind_of => String, :required => true
attribute :checksum, :kind_of => String
attribute :uri, :kind_of => String, :default => "https://github.com/coreos/etcd/releases/download/", :required => true
